const images = [
    {
        src: '..//images/Layer1.png',
        category: 'All',
    },
    {
        src: '..//images/Layer2.png',
        category: 'All',
    },
    {
        src: '..//images/Layer3.png',
        category: 'All',
    },
    {
        src: '..//images/Layer4.png',
        category: 'All',
    },
    {
        src: '..//images/Layer5.png',
        category: 'All',
    },
    {
        src: '..//images/Layer6.png',
        category: 'All',
    },
    {
        src: '..//images/Layer7.png',
        category: 'All',
    },
    {
        src: '..//images/Layer8.png',
        category: 'All',
    },
    {
        src: '..//images/Layer9.png',
        category: 'All',
    },
    {
        src: '..//images/Layer10.png',
        category: 'All',
    },
    {
        src: '..//images/Layer11.png',
        category: 'All',
    },
    {
        src: '..//images/graphicdesign/graphic-design1.jpg',
        category: 'Graphic Design All',
    },
    {
        src: '..//images/graphicdesign/graphic-design2.jpg',
        category: 'Graphic Design All',
    },
    {
        src: '..//images/graphicdesign/graphic-design3.jpg',
        category: 'Graphic Design All',
    },
    {
        src: '..//images/graphicdesign/graphic-design4.jpg',
        category: 'Graphic Design All',
    },
    {
        src: '..//images/graphicdesign/graphic-design5.jpg',
        category: 'Graphic Design All',
    },
    {
        src: '..//images/graphicdesign/graphic-design6.jpg',
        category: 'Graphic Design All',
    },
    {
        src: '..//images/graphicdesign/graphic-design7.jpg',
        category: 'Graphic Design All',
    },
    {
        src: '..//images/graphicdesign/graphic-design8.jpg',
        category: 'Graphic Design All',
    },
    {
        src: '..//images/graphicdesign/graphic-design9.jpg',
        category: 'Graphic Design All',
    },
    {
        src: '..//images/graphicdesign/graphic-design10.jpg',
        category: 'Graphic Design All',
    },
    {
        src: '..//images/graphicdesign/graphic-design11.jpg',
        category: 'Graphic Design All',
    },
    {
        src: '..//images/graphicdesign/graphic-design12.jpg',
        category: 'Graphic Design All',
    },
    {
        src: '..//images/webdesign/web-design1.jpg',
        category: 'Web Design All',
    },
    {
        src: '..//images/webdesign/web-design2.jpg',
        category: 'Web Design All',
    },
    {
        src: '..//images/webdesign/web-design3.jpg',
        category: 'Web Design All',
    },
    {
        src: '..//images/webdesign/web-design4.jpg',
        category: 'Web Design All',
    },
    {
        src: '..//images/webdesign/web-design5.jpg',
        category: 'Web Design All',
    },
    {
        src: '..//images/webdesign/web-design6.jpg',
        category: 'Web Design All',
    },
    {
        src: '..//images/webdesign/web-design7.jpg',
        category: 'Web Design All',
    },
    {
        src: '..//images/landingpage/landing-page1.jpg',
        category: 'Landing Pages All',
    },
    {
        src: '..//images/landingpage/landing-page2.jpg',
        category: 'Landing Pages All',
    },
    {
        src: '..//images/landingpage/landing-page3.jpg',
        category: 'Landing Pages All',
    },
    {
        src: '..//images/landingpage/landing-page4.jpg',
        category: 'Landing Pages All',
    },
    {
        src: '..//images/landingpage/landing-page5.jpg',
        category: 'Landing Pages All',
    },
    {
        src: '..//images/landingpage/landing-page6.jpg',
        category: 'Landing Pages All',
    },
    {
        src: '..//images/landingpage/landing-page7.jpg',
        category: 'Landing Pages All',
    },
    {
        src: '..//images/wordpress/wordpress1.jpg',
        category: 'Wordpress All',
    },
    {
        src: '..//images/wordpress/wordpress2.jpg',
        category: 'Wordpress All',
    },
    {
        src: '..//images/wordpress/wordpress3.jpg',
        category: 'Wordpress All',
    },
    {
        src: '..//images/wordpress/wordpress4.jpg',
        category: 'Wordpress All',
    },
    {
        src: '..//images/wordpress/wordpress5.jpg',
        category: 'Wordpress All',
    },
    {
        src: '..//images/wordpress/wordpress6.jpg',
        category: 'Wordpress All',
    },
    {
        src: '..//images/wordpress/wordpress7.jpg',
        category: 'Wordpress All',
    },
    {
        src: '..//images/wordpress/wordpress8.jpg',
        category: 'Wordpress All',
    },
    {
        src: '..//images/wordpress/wordpress9.jpg',
        category: 'Wordpress All',
    },
    {
        src: '..//images/wordpress/wordpress10.jpg',
        category: 'Wordpress All',
    },
]

/*--------------------"Our service" tabs functional--------------------*/
const tabs = document.querySelector('.navigation');
const tabsContent = document.querySelectorAll('.tab-article');
let currentCategory = 'webDesign';

tabs.addEventListener('click', event => {
    if ( event.target.tagName === 'LI' ) {
        Array.from(tabs.children).forEach(element => {
            element.classList.remove('active');   
        });
        event.target.classList.add('active');
    }

    currentCategory = event.target.dataset.category; 

    tabsContent.forEach( el => {
        el.classList.remove('selected');
        if ( currentCategory === el.dataset.category ) {
            el.classList.add('selected')
        }
    })
});

tabs.addEventListener('mouseover', event => {
    if ( event.target.tagName === 'LI' ) {
        event.target.classList.add('tabs-hovered');
    }
}); 

tabs.addEventListener('mouseout', event => {
    if ( event.target.tagName === 'LI' ) {
        event.target.classList.remove('tabs-hovered');
    }
});


/*--------------------"Our Amazing Work" filter functional--------------------*/

const filterMenu = $('.filter');
const filterContainer = $('.filter-container');
const loadButton = $('#load-more');
const creativeDesignDiv = $('.creative-design').clone(true);
let currentFilterOption = 'All';
let currentArrLength = 12;
const render = (filterCategory = 'All', arrLength = 12) => {
    const renderedArr = images.filter ( el => el.category.indexOf(filterCategory) > -1)
    .map ( el => `<div class='imageWrapper'><img src=${el.src} alt="" class="filtered-img"></img></div>`);
    renderedArr.length = arrLength;
    filterContainer.html(renderedArr.join(''));
    $('.imageWrapper').append(creativeDesignDiv);
};

render();

filterMenu.click (e => {
    if ( e.target !== e.curretTarget) {
        $('.filter-option').removeClass('filter-active');
        e.target.classList.add('filter-active');
    };
    currentFilterOption = e.target.innerText;
    render(currentFilterOption);
});

loadButton.click (e => {
    e.target.classList.add('hidden')
    $('.load-animation').removeClass('hidden');
    currentArrLength += 12;
    setTimeout( () => {
        render(currentFilterOption, currentArrLength);
        if (currentArrLength >= 36){
            e.target.style.display = 'none';
        };
        $('.load-animation').addClass('hidden');
        e.target.classList.remove('hidden');
    }, 2000);
});


/*--------------------"About Ham" opinion functional--------------------*/

const switcher = document.querySelector('.switch');
const userPicture = document.querySelector('.display-picture');
const comments = [
    {
        user: "MARIA MAY",
        comment: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus saepe, eius maxime sint totam tenetur fugiat, porro facilis accusamus fugit vitae hic laudantium doloremque cum velit quis amet veniam atque voluptatibus. Labore eius non inventore qui.',
    },
    {
        user: "JAMES PARKER",
        comment: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam explicabo aut error in praesentium maiores quis! Reiciendis unde iusto saepe distinctio voluptate a sapiente? Dolorum delectus accusantium reprehenderit quidem autem.',
    },
    {
        user: "HASAN ALI",
        comment: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum. Morbi pulvinar odio eget aliquam facilisis.',
    },
    {
        user: "MIA ACKROPOVICH",
        comment: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Omnis ipsum modi temporibus distinctio. Distinctio harum, odit velit deserunt recusandae consequatur inventore quis quaerat molestiae. Iste animi cupiditate atque incidunt laborum temporibus? Dolorem.',
    },
];
const changeText = () => {
    $('.aboute-name').fadeOut(400, () => {
        $('.aboute-name').text($('.current-user .small-diplay-picture').data('user')).fadeIn();
    });
    let userComment = null;
    comments.forEach ( el => {
        if ( el.user ===  $('.current-user .small-diplay-picture').data('user')){
            $('.about-grey-text').fadeOut(400, () => {
                userComment = el.comment;
                $('.about-grey-text').text(userComment).fadeIn();
            })
        }
    })
    
};
const nextUser = () => {
    const currentIndex = $('.small-user').index($('.current-user'));
    $('.small-user.current-user').removeClass('current-user');
    if ( currentIndex >= $('.small-user').length-1 ) {
        $(`.small-user:eq(0)`).addClass('current-user');
    } else {
        $(`.small-user:eq(${currentIndex + 1})`).addClass('current-user');
    }
};
const previousUser = () => {
    const currentIndex = $('.small-user').index($('.current-user'));
    $('.small-user.current-user').removeClass('current-user');
    if ( currentIndex === 0 ) {
        $(`.small-user:eq(${$('.small-user').length - 1})`).addClass('current-user');
    } else {
        $(`.small-user:eq(${currentIndex - 1})`).addClass('current-user');
    }
};
const switchUser = event => {
    Array.from(switcher.children).forEach( el => {
        el.classList.remove('current-user');
    })
    event.target.parentElement.classList.add('current-user');
    const currentUser = document.querySelector('.current-user > .small-diplay-picture');
    userPicture.src = currentUser.src;
    changeText();
};
switcher.addEventListener('click', event => {
    if (event.target.tagName === 'IMG') {
        switchUser(event);
        changeText();
    } else if ( event.target.classList.contains('next') || event.target.classList.contains('switch-png') ) {
        nextUser();
        changeText();
        $('.display-picture').attr('src', $('.current-user > .small-diplay-picture').attr('src'));
    } else if ( event.target.classList.contains('previous')  || event.target.classList.contains('switch-png') ) {
        $('.display-picture').attr('src', $('.current-user > .small-diplay-picture').attr('src'));
        previousUser();
        changeText();
    }
});